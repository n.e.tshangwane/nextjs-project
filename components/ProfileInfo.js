import React from 'react';
import { Card, Button } from 'react-bootstrap';
import Link from 'next/link';
import  Profile  from '../pages/Profile';
import { connect } from 'react-redux';
import profileToViewReducer from '../store/profiles/reducer/ProfileToViewReducer';
import { bindActionCreators } from 'redux';
import styles from './css/ProfileInfo.module.css';
import { getProfileToView } from '../store/profiles/actions/ProfileToViewActions';

const ProfileInfo = ( props) => {

 function onClick(profile) {
    props.getProfileToView(profile);
  }

  return (
      <Card className={styles.user}>
       <Card.Img variant="top" src={props.profile.picture.thumbnail} className="user__image"  />
       <Card.Body>
         <Card.Text className="user__details">
            <span>
               <strong>Name:</strong> {props.profile.name.first}<br/>
            </span>
            <span>
               <strong>Last name:</strong> {props.profile.name.last}<br/>
            </span>
            <span>
               <strong>City:</strong> {props.profile.location.city}<br/>
            </span>
         </Card.Text>
           <Link href='/Profile'>
                <button onClick={() =>
                    onClick(props.profile)}>
                     View Profile
                 </button>
            </Link>
        </Card.Body>
      </Card>
    );
};

const mapDispatchToProps = (dispatch) => {
  return {
    getProfileToView: bindActionCreators(getProfileToView, dispatch)
  }
}

const mapStateToProps = (state) => ({
   profiles: state.profileToViewReducer
});

export default connect(mapStateToProps,mapDispatchToProps)(ProfileInfo)