import React from 'react';
import ProfileInfo from './ProfileInfo';
import styles from './css/ProfileList.module.css';

const ProfileList = ({profiles}) => {
  return (
    <div className={styles.users_list}>
        <React.Fragment>
          {profiles.map((profiles, index) => (
            <ProfileInfo key={index} profile={profiles} />
          ))}
        </React.Fragment>
    </div>
  );
};

export default ProfileList;