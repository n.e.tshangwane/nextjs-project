import styles from './css/Search.module.css'

const SearchControl = ({ handleSearch }) => {
  return (
    <header className={styles.header}>
      <div className={styles.header_search}>
        <input
          className={styles.input}
          type="search"
          placeholder="Search users by first name"
          onChange={handleSearch}
        />
      </div>
    </header>
  );
};

export default SearchControl;