import React from 'react';
import Select from 'react-select';
import styles from './css/Filter.module.css';

const Filters = ({ handleSort, sortOrder }) => {
  const options = [
    { value: '', label: 'None' },
    { value: 'asc', label: 'Ascending' },
    { value: 'desc', label: 'Descending' }
  ];
  return (
    <div  className={styles.sortBy}>
      <Select
        value={sortOrder}
        id="select-filter"
        inputId="select-filter"
        className="select-filter"
        onChange={handleSort}
        placeholder="Sort by age"
        options={options}
      />
    </div>
  );
};

export default Filters;