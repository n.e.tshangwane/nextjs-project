#NextJs with Redux Wrapper App

#How to run
    npm run dev
# Notes
Project divided into 3 folders, components,pages and store.
1. Components contains FilterControl, ProfileList, ProfileInfo and SearchControl
    - FilterControl is responsible for filtering/sorting profiles by age - it uses react-select
    - ProfileInfo is responsible for displaying the details of the profile and it also contains a button that links to the full profile.                Furthermore, when the user clicks the button, it updates the state with the profile to view.
    -  ProfileList is responsible for creating a component for each profile
    - SearchControl is responsible searching for profiles by first name.


2. Store
This contains the state management code. It has the ProfilesReducer which is responsible for updating the profiles. ProfileToViewReducer is responsible for updating the profile of interest. ProfilesActions' purpose is to request the profiles from an API and update the state with the data recieved. Then the ProfileToViewActions is responsible for getting the profile to view. 

3. Pages
Index page contains contains three components SearchControl, FilterControl and ProfileList. It connects to the store and makes use of the ProfilesReducer. The ProfilesReducer is responsible for updating the state of the this page.

Profile is responsible for displaying the full profile. This gets its updat from the ProfileToViewReducer

This app has two separate states - The reason for this is so that the user can go back and forth between index and  profile page without losing any data as a result of state change. This app have made use of the Redux Wrapper to integrate Redux in NextJs

Next step is to tryout other state management tools like Mobx.

