import { Card } from 'react-bootstrap';
import { connect } from 'react-redux'

const Profile = (data) => {
  return (
          <Card className="user">
                 <Card.Img variant="top" src={data.profile.picture.large} className="user__image"  />
                 <Card.Title className="user__name"><strong>{data.profile.name.first} {data.profile.name.last}</strong></Card.Title>
                 <Card.Body>
                   <Card.Text className="user__details">
                        <strong>City:</strong> {data.profile.location.city} <br/>
                         <strong>Age:</strong> {data.profile.dob.age}<br/>
                         <strong>Email:</strong> {data.profile.email}<br/>
                         <strong>Phone:</strong> {data.profile.phone}<br/>
                   </Card.Text>
                </Card.Body>
          </Card>
      );
}
const mapStateToProps = (state) => ({
   profile: state.profileToViewReducer
});

export default connect(mapStateToProps)(Profile)