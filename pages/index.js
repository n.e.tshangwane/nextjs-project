import { useEffect, useState, useRef } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { loadProfiles } from '../store/profiles/actions/ProfilesActions';
import ProfileList from '../components/ProfileList';
import SearchControl from '../components/SearchControl';
import FilterControl from '../components/FilterControl';
import _ from 'lodash';

const index = (props) => {

const inputRef = useRef();
const [profiles, setProfiles] = useState(props.profiles);
const [sortOrder, setSortOrder] = useState('');

  useEffect(() => {
     props.loadProfiles();
     inputRef.current = _.debounce(onSearchText, 500);
  }, [])

  useEffect(() => {
    if (props.profiles.length > 0) {
      updateProfiles(props.profiles);
    }
  }, [props.profiles]);

 function onSearchText(text, props) {
    let filtered;
    if (text) {
      filtered = props.profiles.filter((profile) =>
        profile.name.first.toLowerCase().includes(text.toLowerCase())
      );
    } else {
      filtered = props.profiles;
    }
    updateProfiles(filtered);
    setSortOrder('');
  }

  function updateProfiles(newProfiles) {
    setProfiles(newProfiles);
  }

  function handleSearch(event) {
    inputRef.current(event.target.value, props);
  }

  function handleSort(sortOrder) {
     setSortOrder(sortOrder);
      if (sortOrder.value) {
          updateProfiles(_.orderBy(profiles, ['dob.age'], [sortOrder.value]));
      }
  }
  return (
      <React.Fragment>
        <SearchControl handleSearch={handleSearch} />
        <FilterControl handleSort={handleSort} sortOrder={sortOrder} />
        <ProfileList profiles={profiles}/>
      </React.Fragment>
    );
}

const mapDispatchToProps = (dispatch) => {
  return {
    loadProfiles: bindActionCreators(loadProfiles, dispatch)
  }
}

const mapStateToProps = (state) => ({
  profiles: state.profilesReducer
});

export default connect(mapStateToProps, mapDispatchToProps)(index)
