const ProfilesReducer = (state = [], action) => {
  switch (action.type) {
    case 'SET_PROFILES':
        return [...state, ...action.profiles];
    default:
        return state;
  }
};

export default ProfilesReducer;