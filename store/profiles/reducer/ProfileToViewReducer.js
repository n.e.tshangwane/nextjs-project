const ProfileToViewReducer = (state = [], action) => {
  switch (action.type) {
    case 'SET_PROFILE_TO_VIEW':
        return action.profile;
    default:
        return state;
  }
}
export default ProfileToViewReducer;