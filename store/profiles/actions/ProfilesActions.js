export const loadProfiles = () => {
  return async (dispatch) => {
    try {
      const data = await fetch('https://randomuser.me/api/?results=50');
      const profiles = await data.json();
      return dispatch(setProfiles(profiles.results));
    } catch (error) {
      console.log('error:', error);
    }
  };
};


export const setProfiles = (profiles) => ({
  type: 'SET_PROFILES',
  profiles
});
